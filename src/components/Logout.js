import React from 'react'
import { Redirect } from 'react-router-dom'

const Logout = (props) => {
	props.unsetUser()
	props.emptyCart()
	
	return <Redirect to='/login'/>
}

export default Logout