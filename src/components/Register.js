import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import api from '../api-proxy'
import '../styles/register.css'

document.title = 'Register'

const Register = () => (

	<div className="container-fluid" id="try-photo">

		<div className="row">
			<div className="col-md-6">
					<img src={require('../styles/img/free-diver.jpg')} id="register-explore" />
			</div>
			<div className="col-md-5 mx-auto" id="register-entireform">

				<h4 className="text-center" id="register-label">see the sea - register</h4>

					<div className="card" id="card-register">

						<div className="card-header">Register Information</div>

						<div className="card-body" id="card-body-register">

							<RegisterForm/>

						</div>
						<p className="ml-3"> Have an account? <Link to={'/login'}><b className="text-warning">Login here.</b></Link> </p>
					</div>					
			</div>
		</div>
		
		<div id="background-wrap">
			<div className="bubble x1"></div>
			<div className="bubble x2"></div>
			<div className="bubble x3"></div>
			<div className="bubble x4"></div>
			<div className="bubble x5"></div>
			<div className="bubble x6"></div>
			<div className="bubble x7"></div>
			<div className="bubble x8"></div>
			<div className="bubble x9"></div>
			<div className="bubble x10"></div>
		</div>
		
		
	</div>


	)

class RegisterForm extends Component {

	constructor(props){
		super(props)

		this.state = {
			name: '',
			email:'',
			password:'',
			gotoLogin: false,
			isSubmitDisabled: true
		}
	}

	nameChangeHandler(e) {
		this.setState({
			name: e.target.value
		})
	}

	emailChangeHandler(e) {
		this.setState({
			email: e.target.value
		})
	}

	passwordChangeHandler(e) {

		if(e.target.value.length < 8) {
			this.setState({ isSubmitDisabled: true })
		} else {
			this.setState({ isSubmitDisabled: false })
		}

		this.setState({
			password: e.target.value
		})

	}

	formSubmitHandler(e) {
		e.preventDefault()

		let payload = {
			method: 'post',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: this.state.name,
				email: this.state.email,
				password: this.state.password
			})
		}
		
		fetch(api.url+'/user/register', payload)
		.then((response) => response.json())
		.then((response) => {
			if (response.error !=null) {
				alert(response.error)
			} else {
				this.setState({gotoLogin: true})
			}
		})
	}

	render() {

		if(this.state.gotoLogin) {
			return <Redirect to='/login?register_success=true'/>
		}

		return (
			<form onSubmit={ this.formSubmitHandler.bind(this) }>

				<div className="form-group">

					<input value={ this.state.name } onChange={ this.nameChangeHandler.bind(this) } type="text" className="form-control register-input" placeholder="Enter name" />
					<span className="text-danger"></span>

				</div>

				<div className="form-group">
					<input value={ this.state.email } onChange={ this.emailChangeHandler.bind(this) } type="email" className="form-control mb-1 register-input" placeholder="Enter email"/>
					<span className="text-danger"></span>

				</div>

				<div className="form-group">
					<input value={ this.state.password } onChange={ this.passwordChangeHandler.bind(this) } type="password" className="form-control mt-1 register-input" placeholder="Enter password (must be at least 8 characters)"/>
					<span className="text-danger"></span>

					<button disabled={ this.state.isSubmitDisabled } type="submit" className="btn btn-success btn-block mt-3" id="register-button">Register</button>

				</div>

			</form>
			)
	}

}

export default Register