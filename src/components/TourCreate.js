import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import api from '../api-proxy'
import Swal from 'sweetalert2'

document.title = 'Create Tour'

const TourCreate = () => (
	<div className="container-fluid mt-3">
		<div className="row">
			<div className="col-md-6 col-sm-8 mx-auto">

				<h4 className="text-center">Add Tour</h4>

				<div className="card text-white bg-info">

					<div className="card-header">Tour Information</div>

					<div className="card-body">

						<TourCreateForm/>

					</div>

				</div>

			</div>

		</div>
		<br/><br/>

	</div>
)

class TourCreateForm extends Component {

	constructor(props) {
		super(props)

		this.fileInput = React.createRef()

		this.state = {
			tourName: '',
			description: '',
			unitPrice: '',
			categoryName: undefined,
			categories: [],
			returnToMenu: false
		}
	}

	componentWillMount() {
		fetch(api.url + '/categories')
		.then((response) => response.json())
		.then((categories) => {
			this.setState({ categories: categories })		
		})
	}

	tourNameChangeHandler(e) {
		this.setState({ tourName: e.target.value })
	}

	descriptionChangeHandler(e) {
		this.setState({ description: e.target.value })
	}

	unitPriceChangeHandler(e) {
		this.setState({ unitPrice: e.target.value })
	}

	categoryNameChangeHandler(e) {
		this.setState({ categoryName: e.target.value })
	}

	formSubmitHandler(e) {
		e.preventDefault()

		let payload = {
			method: 'post',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': localStorage.getItem('token')
			},
			body: JSON.stringify({
				name: this.state.tourName,
				description: this.state.description,
				unitPrice: this.state.unitPrice,
				categoryName: this.state.categoryName,
				isArchived: 0
			})
		}

		fetch(api.url + '/tour', payload)
		.then((response) => response.json())
		.then((response) => {
			if(response.error === 'token-expired') {
				Swal.fire({ 
					type: 'error', title: 'Session expired.', 
					text: 'Please login again.'}).then(function() {
					localStorage.clear()
					window.location.href = '/login?session_expired=true'})
			} else if(response.error === 'token-auth-failed'){
				Swal.fire({ 
					type: 'error', title: 'Session expired.', 
					text: 'Please login again.'}).then(function() {
					localStorage.clear()
					window.location.href = '/login?session_expired=true'})
			} else{
				Swal.fire('Success!','Tour added','success')
				this.setState({ returnToMenu: true })
			}

		})
	}

	render() {
		if (this.state.returnToMenu) {
			return <Redirect to='/menu'/>
		}

		return (
			<form onSubmit={ this.formSubmitHandler.bind(this) }>

				<div className="form-group">
					<label>Tour Name</label>
					<input value={ this.state.tourName } onChange={ this.tourNameChangeHandler.bind(this) } type="text" className="form-control" required/>
				</div>

				<div className="form-group">
					<label>Description</label>
					<input value={ this.state.description } onChange={ this.descriptionChangeHandler.bind(this) } type="text" className="form-control"/>
				</div>

				<div className="form-group">
					<label>Unit Price</label>
					<input value={ this.state.unitPrice } onChange={ this.unitPriceChangeHandler.bind(this) } type="number" className="form-control" required/>
				</div>

				<div className="form-group">
					<label>Category</label>
					<select value={ this.state.categoryName } onChange={ this.categoryNameChangeHandler.bind(this) } className="form-control" >
						<option value selected disabled>Select Category</option>
						{
							this.state.categories.map((category) => {
								return <option key={ category._id } value={ category.name }>{ category.name }</option>
							})
						}
					</select>
				</div>

				<div className="form-group">
					<label>Image</label>
					<input type="file" className="form-control" ref={ this.fileInput }/>
				</div>

				<button type="submit" className="btn btn-success btn-block">Add</button>
				<Link className="btn btn-dark btn-block" to="/menu">Cancel</Link>

			</form>
		)
	}

}

export default TourCreate

