import React from 'react'

const About = () => {
	return (
		<div>

			<div className='about-container'>
				<img src={require('../styles/img/diver-paper.jpg')} id="about-banner" alt="dive-banner"/>
				<div className="about-text">
					<h3>dive your worries away</h3>
					We offer boating, snorkeling, freediving, and scuba diving tours <br/>
					along Tiolas Marine Reserve, Iloilo.
				</div>
			</div>

			<div id="background-wrap">
				<div className="bubble x1"></div>
				<div className="bubble x2"></div>
				<div className="bubble x3"></div>
				<div className="bubble x4"></div>
				<div className="bubble x5"></div>
				<div className="bubble x6"></div>
				<div className="bubble x7"></div>
				<div className="bubble x8"></div>
				<div className="bubble x9"></div>
				<div className="bubble x10"></div>
			</div>
		</div>
	)
}

export default About