import React, { Component } from 'react'
import queryString from 'query-string'
import { Redirect, Link } from 'react-router-dom'
import api from '../api-proxy'
import '../styles/login.css'

document.title = 'Login'

const Login = (props) => {

	if(localStorage.getItem('token') != null) {
		return <Redirect to='/menu'/>
	}

	return(
		<div className="container-fluid">
			<div className="row">
					
					<div className="col-md-6">
						<img src={require('../styles/img/divein-login.jpg')} id="login-freedive" />
					</div>
					<div className="col-md-5 mx-auto" id="login-entireform">
							<h4 className="text-center" id="login-label">dive in, log in</h4>
						<div className="card" id="card-login">

							<div className="card-body" id="card-body-login">

								<LoginForm urlParam={ props.location.search } setUser={ props.setUser } />

							</div>
							<p className="ml-4"> No account? <Link to={'/register'}><b className="text-warning">Register here.</b></Link></p>
						</div>	
					</div>
			</div>
			<div id="background-wrap">
				<div className="bubble x1"></div>
				<div className="bubble x2"></div>
				<div className="bubble x3"></div>
				<div className="bubble x4"></div>
				<div className="bubble x5"></div>
				<div className="bubble x6"></div>
				<div className="bubble x7"></div>
				<div className="bubble x8"></div>
				<div className="bubble x9"></div>
				<div className="bubble x10"></div>
			</div>
			
		</div>
	)
	
}

class LoginForm extends Component {

	constructor(props) {
		super(props)

		this.state = {
			email: '',
			password: '',
			errorMessage: '',
			gotoMenu: false
		}
	}

	emailChangeHandler(e) {
		this.setState({ email: e.target.value })
	}

	passwordChangeHandler(e) {
		this.setState({ password: e.target.value })
	}

	formSubmitHandler(e) {
		e.preventDefault()

		let formData = new FormData()
		formData.append('email', this.state.email)
		formData.append('password', this.state.password)

		let payload = { 
			method: 'post', 
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				'email': this.state.email,
				'password': this.state.password
			})
		}

		fetch(api.url+'/user/login', payload)
		.then((response) => response.json())
		.then((response) => {
			if(response.result === 'authenticated') {
				localStorage.setItem('role', response.role)
				localStorage.setItem('name', response.name)
				localStorage.setItem('token', response.token)
				localStorage.setItem('nowBooking', response.nowBooking)
				this.props.setUser()
				this.setState({
					gotoMenu: true
				})
			} else {
				this.setState({
					errorMessage: <div className="alert alert-danger">{ response.error }</div>
				})
			}
		})
	}

	render() {
		if(this.state.gotoMenu)	{
			return <Redirect to='/menu'/>
		}
		//url turn into object
		let url = this.props.urlParam
		let params = queryString.parse(url)
		let registerSuccessMessage = null
		let message = null
		
		if(params.register_success) {
			registerSuccessMessage = <div className="alert alert-success">Registration successful, you may now login.</div>
		}

		if (this.state.errorMessage === '' && registerSuccessMessage != null) {
			message = registerSuccessMessage
		} else {
			message = this.state.errorMessage
		}

		return (
			<form onSubmit={ this.formSubmitHandler.bind(this) }>

				{ message }
								
				<input value={ this.state.email } onChange={ this.emailChangeHandler.bind(this) } type="text" className="form-control my-3" placeholder="Email"/>
				
				<input value={ this.state.password } onChange={ this.passwordChangeHandler.bind(this) } type="password" className="form-control my-3" placeholder="Password"/>
	
				<button type="submit" className="btn btn-success btn-block" id="login-button">Login</button>

			</form>
		)
	}

}

export default Login