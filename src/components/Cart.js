import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'
import api from '../api-proxy'
import Swal from 'sweetalert2'

document.title = 'Cart'

class Cart extends Component {

	constructor(props) {
		super(props)

		this.state = {
			tours: [],
			gotoTransactions: false
		}
	}

	componentWillMount() {
		this.getItems()
	}

	getItems() {
		let payload = {
			method: 'post',
			headers: {
				'Content-Type': 'application/json'
			},
			body: localStorage.getItem('cart')
		}

		fetch(api.url + '/cart/info', payload)
		.then((response) => response.json())
		.then((cartTours) => {
			this.setState({ tours: cartTours })
		})
	}

	emptyCart() {
		localStorage.removeItem('cart')
		this.props.getTotalCartQuantity()
		this.setState({ tours: [] })
		localStorage.setItem('nowBooking', 'false')
	}

	proceedToCheckout() {
		let payload = {
			method: 'post',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': localStorage.getItem('token')
			},
			body: JSON.stringify({
				cart: localStorage.getItem('cart')
			})
		}

		fetch(api.url + '/cart/checkout', payload)
		.then((response) => response.json())
		.then((response) => {
			if(response.error === 'token-expired') {
				Swal.fire({ 
					type: 'error', title: 'Session expired', 
					text: 'Please login again.'}).then(function() {
					localStorage.clear()
					window.location.href = '/login?session_expired=true'})
			} else if(response.error === 'token-auth-failed'){
				Swal.fire({ 
					type: 'error', title: 'Session expired', 
					text: 'Please login again.'}).then(function() {
					localStorage.clear()
					window.location.href = '/login?session_expired=true'})
			} else if(response.result === 'success') {
				Swal.fire('Booked successfully')
				this.setState({ gotoTransactions: true })
				this.emptyCart()
			} else {
				alert(response.error)
			}
		})
		
	}

	proceedToStripeCheckout() {
		let payload = {
			method: 'post',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': localStorage.getItem('token')
			},
			body: JSON.stringify({
				cart: localStorage.getItem('cart')
			})
		}


		fetch(api.url + '/cart/checkout-stripe', payload)
		.then((response) => response.json())
		.then((response) => {
			if(response.error === 'token-expired') {
				Swal.fire({ 
					type: 'error', title: 'Session expired', 
					text: 'Please login again.'}).then(function() {
					localStorage.clear()
					window.location.href = '/login?session_expired=true'})
			} else if(response.error === 'token-auth-failed'){
				Swal.fire({ 
					type: 'error', title: 'Session expired', 
					text: 'Please login again.'}).then(function() {
					localStorage.clear()
					window.location.href = '/login?session_expired=true'})
			} else if(response.result === 'success') {
				Swal.fire('Stripe checkout successful')
				this.setState({ gotoTransactions: true })
				this.emptyCart()
			} else {
				alert(response.error)
			}
			
		})
	}

	render() {
		if (this.state.gotoTransactions) {
			return <Redirect to='/transactions'/>
		}

		if (this.state.tours.length === 0) {
			localStorage.setItem('nowBooking', 'false')
			return (
				<div className="container-fluid mt-4">
					<h4> Cart is currently empty. </h4>
					<h5> Select a tour to add to cart from <Link to="/menu">here</Link>.</h5>
				</div>
			)
		}

		let totalPrice = 0;

		localStorage.setItem('nowBooking', 'true')
		this.state.tours.map((tour) => {
			return ( totalPrice += (tour.quantity * tour.unitPrice) )
		})

		return (
			<div className="container-fluid mt-3">

				<h4>Cart</h4>

				<table className="table table-bordered">

					<thead>

						<tr> 
							<th>Tour</th>
							<th>Price per Person</th>
							<th>Tour Date</th>
							<th>Number of Guests <i>(max 12)</i></th>
						</tr>

					</thead>

					<tbody>

						<CartList getItems={ this.getItems.bind(this) } tours={ this.state.tours } getTotalCartQuantity={ this.props.getTotalCartQuantity }/>
						
						<tr> 
							<th colSpan="3" className="text-right">Total</th>
							<th className="text-right">&#8369; { totalPrice.toFixed(2) }</th>
						</tr>

					</tbody>

				</table>

				<button className="btn btn-danger mr-3 my-2" onClick={ this.emptyCart.bind(this) }>Empty Cart</button>
				<button className="btn btn-success mr-3 my-2" onClick={ this.proceedToCheckout.bind(this) }>Proceed to Checkout</button>
				<button className="btn btn-success my-2" onClick={ this.proceedToStripeCheckout.bind(this) }>Proceed to Checkout (Stripe)</button>

			</div>
		)
	}
}

const CartList = (props) => {
	return (
		props.tours.map((tour) => {
			return (
				<tr key={ tour._id }>
					<td>{ tour.name }</td>
					<td>&#8369; { (tour.unitPrice).toFixed(2) }</td>
					<td> {tour.bookDate}</td>
					<td>
						<UpdateTourInput getItems={ props.getItems } _id={ tour._id } getTotalCartQuantity={ props.getTotalCartQuantity } quantity={ tour.quantity }/>
					</td>
					
				</tr>
			)
		})
	)
}



class UpdateTourInput extends Component {

	constructor(props) {
		super(props) 

		this.state = {
			quantity: props.quantity
		}
	}

	updateQuantity(e) {
		let cart = JSON.parse(localStorage.getItem('cart'))

		for (let i = 0; i < cart.length; i++) {
			if (cart[i]._id === this.props._id) {
				cart[i].quantity = parseFloat(e.target.value)
			}
		}

		localStorage.setItem('cart', JSON.stringify(cart))

		this.setState({ quantity: e.target.value })
		this.props.getTotalCartQuantity()
		this.props.getItems()
	}

	render() {
		return (
			<input value={ this.state.quantity } onChange={ this.updateQuantity.bind(this) } type="number" className="form-control" min="1" max="12"/>
		)
	}

}

export default Cart

