import React, { Component, Fragment } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Page from 'react-page-loading'
import '../styles/app.css'

import Footer from './Footer'
import Header from './Header'

import About from './About'
import Menu from './Menu'
import Register from './Register'
import Login from './Login'
import Logout from './Logout'
import TourCreate from './TourCreate'
import TourUpdate from './TourUpdate'
import TourDelete from './TourDelete'
import TourBook from './TourBook'
import Cart from './Cart'
import Transactions from './Transactions'
import History from './History'

class App extends Component {

   constructor(props) {
      super(props)

      this.state = {
         name: localStorage.getItem('name'),
         role: localStorage.getItem('role'),
         token: localStorage.getItem('token'),
         nowBooking: localStorage.getItem('nowBooking'),
         cartQuantity: 0,
         bookQuantity: 0,
         loading: true
      }
   }

   componentDidMount() {
      this.getTotalCartQuantity()
      demoAsyncCall().then(() => this.setState({ loading: false })); //
   }

   setUser() {
      this.setState({
         name: localStorage.getItem('name'),
         role: localStorage.getItem('role'),
         token: localStorage.getItem('token'),
         nowBooking: localStorage.getItem('nowBooking')
      })
   }

   unsetUser() {
      localStorage.clear()

      this.setState({
         name: localStorage.getItem('name'),
         role: localStorage.getItem('role'),
         token: localStorage.getItem('token'),
         nowBooking: localStorage.getItem('nowBooking')
      })
   }

   emptyCart() {
      this.setState({
         cartQuantity: 0
      })
   }

   getTotalCartQuantity() {
      let cartQuantity = 0
      let cart = JSON.parse(localStorage.getItem('cart'))

      if (cart != null) {
         for (let i = 0; i< cart.length; i++) {
            cartQuantity += parseFloat(cart[i].quantity)
         }
      }

      this.setState({ cartQuantity: cartQuantity })
   }

   render() {

      const spinner = document.getElementById('spinner');

      if (spinner && !spinner.hasAttribute('hidden')) {
      spinner.setAttribute('hidden', 'true');
      }

      const { loading } = this.state //
      if (loading) {
         return (
            <div>
               <Page loader={"bubble-spin"} color={"#ADD8E6"} size={20}> </Page>
            </div>
         );
      }
      let LoginComponent = (props) => (<Login {...props} setUser={ this.setUser.bind(this) }/>)
      let LogoutComponent = (props) => (<Logout {...props} unsetUser={ this.unsetUser.bind(this) } emptyCart={ this.emptyCart.bind(this) } />)
      let TourBookComponent = (props) => (<TourBook {...props} getTotalCartQuantity = { this.getTotalCartQuantity.bind(this) }/>)
      let CartComponent = (props) => (<Cart {...props} getTotalCartQuantity={ this.getTotalCartQuantity.bind(this) } />)
    
      return (
         
         <Fragment>
            <BrowserRouter>
            <Header token={ this.state.token } name={ this.state.name } role={ this.state.role} cartQuantity={ this.state.cartQuantity }/>
          
               <Switch>
                  <Route exact path='/' component={ About }/>
                  <Route exact path='/menu' component={ Menu }/>
                  <Route exact path='/register' component={ Register }/>
                  <Route exact path='/login' render={ LoginComponent }/>
                  <Route exact path='/logout' render={ LogoutComponent }/>
                  <Route exact path='/tour-book' render={ TourBookComponent }/>
                  <Route exact path='/tour-create' component={ TourCreate }/>
                  <Route exact path='/tour-update' component={ TourUpdate }/>
                  <Route exact path='/tour-delete' component={ TourDelete }/>
                  <Route exact path='/cart' render={ CartComponent }/>
                  <Route exact path='/transactions' component={ Transactions }/>            
                  <Route exact path='/history' component={ History }/>            
               </Switch>
            
            </BrowserRouter>
            <Footer/>
         </Fragment>
      )
   }

}

function demoAsyncCall() {
  return new Promise((resolve) => setTimeout(() => resolve(), 800));
}

export default App