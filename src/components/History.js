import React, { Component } from 'react'
import api from '../api-proxy'
import Swal from 'sweetalert2'
document.title = 'Tiolas Sea Tours'

class History extends Component {

	constructor(props) {
		super(props)

		this.state = {
			orders: []
		}
	}

	componentWillMount() {
		let payload = {
			method: 'get',
			headers: {
				'Authorization': localStorage.getItem('token')
			}
		}


		fetch(api.url + '/orders/admin', payload)
		.then((response) => response.json())
		.then((response) => {
			
			if(response.error === 'token-expired') {
				Swal.fire({ 
					type: 'error', title: 'Session expired', 
					text: 'Please login again.'}).then(function() {
					localStorage.clear()
					window.location.href = '/login?session_expired=true'})
			} else if(response.error === 'token-auth-failed'){
				Swal.fire({ 
					type: 'error', title: 'Session expired', 
					text: 'Please login again.'}).then(function() {
					localStorage.clear()
					window.location.href = '/login?session_expired=true'})
			}  else if(response.result === 'success') {
				this.setState({ orders: response.orders })
			} else {
				alert(response.error)
			}
		})	
	}

	

	render() {
		return (
			<div className="container-fluid mt-3">

				<h4 className="my-3">Transaction History for All Users</h4>

				<table className="table table-bordered table-responsive-sm">

					<thead>
						<tr className="text-center">
							<th>Tour Name </th>
							<th>Tour Date</th>
							<th>Transaction ID</th>
							<th>Guest Count </th>
							<th>Total Price</th>
							<th>Payment Mode</th>
							<th>Order Created</th>
						</tr>
					</thead>

					<tbody>
						{
							this.state.orders.map((order)=> {
								return <tr className="text-center">
									<td>{ order.tours.map((tour) => { return(tour.name)} )} </td>
									<td>{ order.tours.map((tour) => { 
											let date = tour.bookDate.toLocaleString()
											return date.substring(0,10)  } )}</td>
									<td>{ order._id }</td>
									<td>{ order.tours.map((tour) => { return(tour.quantity)} )} </td>
									<td className="text-right">&#8369;{ order.totalPrice.toFixed(2) }</td>		
									<td>{ order.paymentMode }</td>
									<td><i>{ new Date(order.datetimeRecorded).toLocaleString() }</i></td>
								</tr>
							})
							
						}
					</tbody>

				</table>
				<br/><br/>
				
			</div>
		)
	}
}

export default History

