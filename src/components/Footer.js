import React from 'react'

const Footer = () => {
	return (
		<footer className="bg-dark fixed-bottom" id="footer-style" >
			
			<p className="text-center py-2 text-white my-0" style={{fontSize:"14px"}}>
			Copyright &copy; 2019 CoffeePoweredEyps
			</p>
		</footer>
	)
}

export default Footer