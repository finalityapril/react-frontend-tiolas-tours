import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import api from '../api-proxy'

const Menu = (props) => {
	document.title = 'Tiolas Sea Tours'
	let btnAddItem = null

	if (localStorage.getItem('role') === 'admin') {
		btnAddItem = <Link className="btn btn-sm btn-primary" to="/tour-create">Add Item</Link>
	}

	return (
		<div className="container-fluid mt-3">
			<h4>Available Tours</h4>
			{ btnAddItem }
			<MenuList getTotalCartQuantity={ props.getTotalCartQuantity }/>
			<br/><br/>
		</div>
	)	
}

class MenuList extends Component {

	constructor(props) {
		super(props)

		this.state = {
			tours: []
		}
	}

	componentWillMount() {
		fetch(api.url + '/tours')
		.then((response) => response.json())
		.then((tours) => {
			this.setState({ tours: tours })
		})
	}

	render() {
		return (
			<div className="row mt-3">
			{
				this.state.tours.map((tour) => {

					const nowBooking = localStorage.getItem('nowBooking')
					return (
						<div key={ tour._id } className="col-sm-3 mb-3">
							<div className="card" id="menu-card">
								<img className="card-img-top" width="100%" height="200px" style={ {objectFit: 'cover'} } src={ tour.imageLocation } alt='' />
									<div className="card-body">

										<h4 className="card-title">{ tour.name }</h4>

										<p className="card-text">{ tour.description }</p>

										<p className="card-text">&#8369;{ tour.unitPrice }</p>

										{
										
											(localStorage.getItem('role') === 'admin') ? 
											(
												<div className="btn-group btn-block">
												<Link className="btn btn-info" to={"/tour-update?_id="+tour._id }>Edit</Link>
												<Link className="btn btn-danger" to={"/tour-delete?_id="+tour._id }>Delete</Link>
												</div>
											) : 
											(localStorage.getItem('role') === 'customer') ?
											(
												( nowBooking === 'true') ?
												( <Link className="btn btn-secondary" to={"/cart"}>Please finish or cancel current booking. </Link> ) :
												( <Link className="btn btn-primary" to={"/tour-book?_id="+tour._id }>Add this tour</Link> )
											) :
											(<Link className="btn btn-secondary" to={"/login"}>Please login to book this tour </Link>)
										}

								</div>

							</div>

						</div>
						)
				})
			}
			</div>
		
			)
	}
}



export default Menu