import React from 'react'
import { Link } from 'react-router-dom'
import '../styles/headerfooter.css'

const Header = (props) => {

	let navRight = (localStorage.getItem('token') === null) ?
		(
			<React.Fragment>
			<li className="nav-item">
				<Link className="nav-link text-white" to="/login">Login</Link>
			</li>

			<li className="nav-item">
				<Link className="nav-link text-white" to="/register">Register</Link>
			</li>
			</React.Fragment>
		) :
		(
			<React.Fragment>
			<li className="nav-item">
				<Link className="nav-link text-warning" to="/menu">{ localStorage.getItem('name') }</Link>
			</li>

			 { (localStorage.getItem('role') === 'admin') ? 
			 	(
				<li className="nav-item">
					<Link className="nav-link text-white" to="/history">History</Link>
				</li>
				):
				(
				<li className="nav-item">
					<Link className="nav-link text-white" to="/transactions">Transactions</Link>
				</li>	
				)
			 }


			<li className="nav-item">
				<Link className="nav-link text-white" to="/logout">Logout</Link>
			</li>
			</React.Fragment>
		)

	return(
		
		<nav className="navbar navbar-expand-lg navbar-dark">
			
			<Link className="navbar-brand" to="/"> 
			
			Tiolas Sea Tours</Link>

			<button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar">
				<span className="navbar-toggler-icon"></span>
			</button>

			<div className="collapse navbar-collapse" id="navbar">

				<ul className="navbar-nav mr-auto">

					<li className="nav-item">
						<Link className="nav-link" to="/">About</Link>
					</li>

					<li className="nav-item">
						<Link className="nav-link" to="/menu">Menu</Link>
					</li>
					
					{ (localStorage.getItem('role') === 'customer') ? 		
					  	(<li className="nav-item">
							<Link className="nav-link" to="/cart">Cart <span className="badge badge-light">{ props.cartQuantity }</span></Link>
						</li>):
						(<li></li>)
					}

				</ul>

				<ul className="navbar-nav ml-auto">

					{ navRight }

				</ul>

			</div>

		</nav>
	
		
		
		
	)
}

export default Header