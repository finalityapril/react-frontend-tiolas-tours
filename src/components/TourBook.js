import React, { Component } from 'react'
import queryString from 'query-string'
import { Redirect, Link } from 'react-router-dom'
import api from '../api-proxy'
import Swal from 'sweetalert2'

document.title = 'Book Tour'

const TourBook = (props) => (
	<div className="container-fluid mt-3">

		<div className="row">

			<div className="col-md-6 col-sm-8 mx-auto">

				<h4 className="text-center">Book Tour</h4>

				<div className="card bg-info">

					<div className="card-header">Tour Information</div>

					<div className="card-body">

						<TourBookForm urlParam={ props.location.search } getTotalCartQuantity={ props.getTotalCartQuantity }/>

					</div>
				</div>
			</div>
		</div>
		<br/><br/>
	</div>
)

class TourBookForm extends Component {

	constructor(props) {
		super(props)

		this.fileInput = React.createRef()
		let params = queryString.parse(this.props.urlParam)

		this.state = {
			_id: params._id,
			tourName: '',
			description: '',
			unitPrice: '',
			categoryName: undefined,
			categories: [],
			returnToMenu: false,
			quantity: 0,
			bookDate: ''
		}
	}

	componentWillMount() {
		let payload = {
			method: 'get',
			headers: {
				'Authorization': localStorage.getItem('token')
			}
		}


		fetch(api.url + '/bookcategory', payload)
		.then((response) => response.json())
		.then((response) => {
			if(response.error === 'token-expired') {
				Swal.fire({ 
					type: 'error', title: 'Booking failed', 
					text: 'Session expired. Please login again.'}).then(function() {
					localStorage.clear()
					window.location.href = '/login?session_expired=true'})
			} else if(response.error === 'token-auth-failed') {
				Swal.fire({
					type: 'error', title: 'Booking failed', 
					text: 'Session expired. Please login again.'}).then(function() {
					localStorage.clear()
					window.location.href = '/login?session_expired=true'})
			} else {
				this.setState({ categories: response.categories })
				fetch(api.url + '/tour/' + this.state._id)
				.then((response) => response.json())
				.then((tour) => {
					this.setState({ 
						tourName: tour.name,
						description: tour.description,
						unitPrice: tour.unitPrice,
						categoryName: tour.categoryName
					})
				})
			}

		})
	}

	quantityChangeHandler(e) {
		this.setState({ quantity: e.target.value })
	}

	bookDateChangeHandler(e) {
		this.setState({ bookDate: e.target.value })
	}

	formSubmitHandler(e) {
		e.preventDefault()

		let cart = JSON.parse(localStorage.getItem('cart'))

		if (cart != null) {
			for (let i = 0; i < cart.length; i++) {
				if (cart[i]._id === this.state._id) {
					cart[i].quantity = parseFloat(cart[i].quantity) + parseFloat(this.state.quantity)
				
				localStorage.setItem('cart', JSON.stringify(cart))


				this.setState({ quantity: 0 })
				this.props.getTotalCartQuantity()

				return

				}
			}

			cart.push({
				'_id': this.state._id,
				'quantity': parseFloat(this.state.quantity),
				'bookDate': this.state.bookDate

			})

		} else {
	
			cart = []
			cart.push({
				'_id': this.state._id,
				'quantity': parseFloat(this.state.quantity),
				'bookDate': this.state.bookDate
			})
		}


		localStorage.setItem('cart', JSON.stringify(cart))
		Swal.fire('Success!',' Tour added to cart.','success')

		this.setState({ quantity: 0, returnToMenu: true })
		this.props.getTotalCartQuantity()

	}

	render() {
		if (this.state.returnToMenu === true) {
			return <Redirect to='/cart'/>
		}

		return (
			<form onSubmit={ this.formSubmitHandler.bind(this) }>

				<div className="form-group">
					<label>Tour Name</label>
					<input value={ this.state.tourName } type="text" className="form-control" readOnly/>
				</div>

				<div className="form-group">
					<label>Description</label>
					<input value={ this.state.description } type="text" className="form-control" readOnly/>
				</div>

				<div className="form-group">
					<label>Unit Price</label>
					<input value={ this.state.unitPrice } type="number" className="form-control" readOnly/>
				</div>

				<div className="form-group">
					<label>Number of Guests <i>(max 12)</i></label>
					<input value={ this.state.quantity } onChange={ this.quantityChangeHandler.bind(this) } type="number" className="form-control" min="1" max="12"/>
				</div>

				<div className= "form-group">
					<label>Date</label>
					<input type="date" onChange={ this.bookDateChangeHandler.bind(this) }className="form-control" required/>
				</div>


				<button type="submit" className="btn btn-success btn-block">Book</button>
				<Link className="btn btn-warning btn-block" to="/menu">Cancel</Link>

			</form>
		)
	}

}


export default TourBook