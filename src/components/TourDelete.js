import React, { Component } from 'react'
import queryString from 'query-string'
import { Redirect, Link } from 'react-router-dom'
import api from '../api-proxy'
import Swal from 'sweetalert2'

document.title = 'Delete Tour'

const TourDelete = (props) => (
	<div className="container-fluid mt-3">

		<div className="row">

			<div className="col-md-6 col-sm-8 mx-auto">

				<h4 className="text-center">Delete Tour</h4>

				<div className="card text-white bg-info">

					<div className="card-header">Tour Information</div>

					<div className="card-body">

						<TourDeleteForm urlParam={ props.location.search } />

					</div>

				</div>

			</div>

		</div>
		<br/><br/>

	</div>
)

class TourDeleteForm extends Component {

	constructor(props) {
		super(props)

		let params = queryString.parse(this.props.urlParam)

		this.state = {
			_id: params._id,
			tourName: '',
			description: '',
			unitPrice: '',
			categoryName: undefined,
			categories: [],
			returnToMenu: false
		}
	}

	componentWillMount() {
		fetch(api.url + '/categories')
		.then((response) => response.json())
		.then((categories) => {
			this.setState({ categories: categories })	

			fetch(api.url + '/tour/' + this.state._id)
			.then((response) => response.json())
			.then((tour) => {
				this.setState({ 
					tourName: tour.name,
					description: tour.description,
					unitPrice: tour.unitPrice,
					categoryName: tour.categoryName
				})
			})
		})
	}

	formSubmitHandler(e) {
		e.preventDefault()

		let payload = {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': localStorage.getItem('token')
			},
			body: JSON.stringify({
				'_id': this.state._id
			})
		}

		fetch(api.url + '/tour', payload)
		.then((response) => response.json())
		.then((response) => {
			if(response.error === 'token-expired') {
				Swal.fire({ 
					type: 'error', title: 'Delete failed!', 
					text: 'Session expired, please login again!'}).then(function() {
					localStorage.clear()
					window.location.href = '/login?session_expired=true'})
			} else if(response.error === 'token-auth-failed'){
				Swal.fire({ 
					type: 'error', title: 'Delete failed!', 
					text: 'Session expired, please login again!'}).then(function() {
					localStorage.clear()
					window.location.href = '/login?session_expired=true'})
			} else if(response.result === 'success') {
				Swal.fire('Delete successful')
				this.setState({ returnToMenu: true })
			} else {
				alert(response.error)
			}
		})
	}

	render() {
		if (this.state.returnToMenu) {
			return <Redirect to='/menu'/>
		}

		return (
			<form onSubmit={ this.formSubmitHandler.bind(this) }>

				<div className="form-group">
					<label>Tour Name</label>
					<input value={ this.state.tourName } type="text" className="form-control" readOnly/>
				</div>

				<div className="form-group">
					<label>Description</label>
					<input value={ this.state.description } type="text" className="form-control" readOnly/>
				</div>

				<div className="form-group">
					<label>Unit Price</label>
					<input value={ this.state.unitPrice } type="number" className="form-control"  readOnly/>
				</div>

				<div className="form-group">
					<label>Category</label>
					<select value={ this.state.categoryName } className="form-control" readOnly >
						<option value disabled>Select Category</option>
						{
							this.state.categories.map((category) => {
								return <option key={ category.id } value= { category.name }>{ category.name }</option>
							})
						}
					</select>
				</div>

				<button type="submit" className="btn btn-danger btn-block">Delete</button>
				<Link className="btn btn-dark btn-block" to="/menu">Cancel</Link>

			</form>
		)
	}
}

export default TourDelete

