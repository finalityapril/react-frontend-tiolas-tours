import React, { Component } from 'react'
import queryString from 'query-string'
import { Redirect, Link } from 'react-router-dom'
import api from '../api-proxy'
import Swal from 'sweetalert2'

document.title = 'Update Tour'

const TourUpdate = (props) => (
	<div className="container-fluid mt-3">

        <div className="row">

            <div className="col-md-6 col-sm-8 mx-auto">

                <h4 className="text-center">Update Tour</h4>

                <div className="card text-white bg-info">

                    <div className="card-header">Tour Information</div>

                    <div className="card-body">

						<TourUpdateForm urlParam={ props.location.search }/>

                    </div>

                </div>

            </div>

        </div>
        <br/><br/>

    </div>
)

class TourUpdateForm extends Component {

	constructor(props) {
		super(props)

		this.fileInput = React.createRef()
		let params = queryString.parse(this.props.urlParam)

		this.state = {
			_id: params._id,
			tourName: '',
			description: '',
			unitPrice: '',
			categoryName: undefined,
			categories: [],
			returnToMenu: false
		}
	}

	componentWillMount() {
		fetch(api.url + '/categories')
		.then((response) => response.json())
		.then((categories) => {
			this.setState({ categories: categories })

			fetch(api.url + '/tour/' + this.state._id)
			.then((response) => response.json())
			.then((tour) => {
				this.setState({ 
					tourName: tour.name,
					description: tour.description,
					unitPrice: tour.unitPrice,
					categoryName: tour.categoryName
				})
			})		
		})
	}

	tourNameChangeHandler(e) {
		this.setState({ tourName: e.target.value })
	}

	descriptionChangeHandler(e) {
		this.setState({ description: e.target.value })
	}

	unitPriceChangeHandler(e) {
		this.setState({ unitPrice: e.target.value })
	}

	categoryNameChangeHandler(e) {
		this.setState({ categoryName: e.target.value })
	}

	formSubmitHandler(e) {
		e.preventDefault()

		let payload = {
			method: 'put',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': localStorage.getItem('token')

			},
			body: JSON.stringify({
				_id: this.state._id,
				name: this.state.tourName,
				description: this.state.description,
				unitPrice: this.state.unitPrice,
				categoryName: this.state.categoryName,
				isArchived: 0
			})
		}

		fetch(api.url + '/tour', payload)
		.then((response) => response.json())
		.then((response) => {
			if(response.error === 'token-expired') {
				Swal.fire({ 
					type: 'error', title: 'Session expired', 
					text: 'Please login again.'}).then(function() {
					localStorage.clear()
					window.location.href = '/login?session_expired=true'})
			} else if(response.error === 'token-auth-failed'){
				Swal.fire({ 
					type: 'error', title: 'Session expired', 
					text: 'Please login again.'}).then(function() {
					localStorage.clear()
					window.location.href = '/login?session_expired=true'})
			} else {
				Swal.fire('Success!','Tour details updated.','success')
				this.setState({ returnToMenu: true })
			}
		})
	}


	render() {
		if (this.state.returnToMenu) {
			return <Redirect to='/menu'/>
		}

		return (
			<form onSubmit={ this.formSubmitHandler.bind(this) }>

				<div className="form-group">
					<label>Tour Name</label>
					<input value={ this.state.tourName } onChange={ this.tourNameChangeHandler.bind(this) } type="text" className="form-control" required/>
				</div>

				<div className="form-group">
					<label>Description</label>
					<input value={ this.state.description } onChange={ this.descriptionChangeHandler.bind(this) } type="text" className="form-control"/>
				</div>

				<div className="form-group">
					<label>Unit Price</label>
					<input value={ this.state.unitPrice } onChange={ this.unitPriceChangeHandler.bind(this) } type="number" className="form-control" required/>
				</div>

				<div className="form-group">
					<label>Category</label>
					<select value={ this.state.categoryName } onChange={ this.categoryNameChangeHandler.bind(this) } className="form-control" >
						<option value disabled>Select Category</option>
						{
							this.state.categories.map((category) => {
								return <option key={ category._id } value= { category.name }>{ category.name }</option>
							})
						}
					</select>
				</div>

				<div className="form-group">
					<label>Image</label>
					<input type="file" className="form-control" ref={ this.fileInput } />
				</div>

				<button type="submit" className="btn btn-success btn-block">Update</button>
				<Link className="btn btn-dark btn-block" to="/menu">Cancel</Link>

			</form>
		)
	}

}

export default TourUpdate


